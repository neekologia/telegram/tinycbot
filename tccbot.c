#include <stdio.h>
#include <string.h>

#include <cjson/cJSON.h>
#include <curl/curl.h>

char* read_to_string(char *file, int bufsz) {
	char string[bufsz];
	FILE *fp = fopen(file, "r");
	fgets(string, bufsz, fp);
	fclose(fp);
	return string;
}

void send_message(char *chat_id, char *text) {
	char api[BUFSIZ];
	
	char url[] = "http://192.168.1.15:8081/bot";
	strcpy(api, url);

	int bufsz = 48;
	char token[bufsz];
	token = read_to_string("token", bufsz);
	token[strlen(token)-1] = '\0';

	strcat(api, token);
	strcat(api, "/sendMessage");
	
	printf("api: %s\n", api);

	CURL *curl = curl_easy_init();
	curl_easy_setopt(
		curl, CURLOPT_URL, api);

	struct curl_httppost* post = NULL;
	struct curl_httppost* last = NULL;
	curl_formadd(&post, &last,
		CURLFORM_COPYNAME, "chat_id",
		CURLFORM_COPYCONTENTS, chat_id, CURLFORM_END);
	curl_formadd(&post, &last,
		CURLFORM_COPYNAME, "text",
		CURLFORM_COPYCONTENTS, text, CURLFORM_END);
	curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);

	curl_easy_perform(curl);
	curl_easy_cleanup(curl);
}

int main(int argc, char *argv[]) {
	cJSON *input = cJSON_Parse(argv[1]);
	cJSON *update_id = cJSON_GetObjectItem(input, "update_id");
	char *update_id_str = cJSON_Print(update_id);
	printf("id: %s ", update_id_str);
	if (cJSON_HasObjectItem(input, "message")) {
		cJSON *message = cJSON_GetObjectItem(input, "message");
		char *chat_id = cJSON_Print(
			cJSON_GetObjectItem(
				cJSON_GetObjectItem(message, "chat"),
				"id"
			)
		);
		if (cJSON_HasObjectItem(message, "text")) {
			char *text = cJSON_GetStringValue(
				cJSON_GetObjectItem(message, "text"));
			printf("%s:%s\n", chat_id, text);
			send_message(chat_id, text);
		}
	}

	printf("\n\n");
	return 0;
}
